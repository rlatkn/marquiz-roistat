FROM node:9.4

WORKDIR /app

ADD package.json ./
ADD yarn.lock ./
RUN yarn

ADD . ./

CMD node .
EXPOSE 3000