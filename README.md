# Script for transfer leads from Marquiz to Roistat

## Configuration
    cp .env.example .env

  and configure it

## Running with docker
```
docker build -t marquiz-roistat .
docker run -d -p 3000:3000 --name marquiz-roistat --restart=on-failure marquiz-roistat
```
### STOP
    docker stop marquiz-roistat

### Remove (use it to rebuild container)

    docker rm -f marquiz-roistat
  
  and repeat running

## Native run (without docker)

install Node.js and
```
yarn
node .
```