const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');

require('dotenv').config()

const app = express();
app.use(bodyParser.json());

app.post('/roistat', function(request, response) {
  response.set('Content-Type', 'application/json');
  const lead = request.body;
  const apiKey = request.query.apiKey

  axios.get('https://cloud.roistat.com/api/proxy/1.0/leads/add', {
    params: {
      roistat: lead.extra.cookies['roistat_visit'],
      key: apiKey,
      title: 'Заявка из marquiz.ru',
      comment: null,
      name: lead.contacts.name,
      phone: lead.contacts.phone,
      email: lead.contacts.email,
      is_need_callback: 0,
      callback_phone: '',
      sync: 0,
      is_need_check_order_in_processing: 1,
      is_need_check_order_in_processing_append: 1,
      // fields: {},
    },
  }).then(res => {
    if (res.data.status === 'error') {
      throw new Error(res.data.data);
    }
    response.status(201).send(res.data);
  }).catch(e => {
    response.status(400).send({ error: e.message });
  });
});

app.listen(process.env.PORT || 3000, function () {
  console.log('App started');
});
